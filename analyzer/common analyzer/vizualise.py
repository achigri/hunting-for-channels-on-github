import pandas as pd
import os
import matplotlib.pyplot as plt

df = pd.DataFrame()
total_nb_proj = 0
proj_with_one_chan = 0
proj_with_one_goroutine = 0

# Go Data
# Preview the first 5 lines of the loaded data

total_nb_proj_go = sum(1 for x in  os.walk('output-go'))
total_nb_line_go = 0
for dirpath, _, _ in os.walk('output-go'):
   if dirpath != 'output-go':
       data_lines = pd.read_csv(dirpath + "/result-lines.csv")
       total_nb_line_go += data_lines.iloc[:, -1:].sum(axis=0).sum(axis=0)
print("total nb lines go", total_nb_line_go)
avg_nb_of_line_go = total_nb_line_go / total_nb_proj_go
print("avg line per proj, go: ", avg_nb_of_line_go)
for dirpath, _, _ in os.walk('output-go'):
   if dirpath != 'output-go':
      data_per_channel = pd.read_csv(dirpath + "/result-channels.csv")
      #data_per_function = pd.read_csv(dirpath + "/result-functions.csv")
      data_goroutines = pd.read_csv(dirpath + "/result-goroutines-lines.csv")
      data_lines = pd.read_csv(dirpath + "/result-lines.csv")
      nb_functions = int(pd.read_csv(dirpath + "/number_of_functions.csv").keys()[1])

      if nb_functions != 0:
         total_nb_proj += 1
         nb_channels = data_per_channel.shape[0] # get number of rows in file. Because 1 row = 1 channel
         total_nb_of_operations = data_per_channel.iloc[:, -9:-4].sum(axis=0).sum(axis=0) # total number of operations without for loops
         avg_nb_of_operations_per_func = data_per_channel.groupby(['package name', 'function name']).agg({
            'make count':'sum', 'send stmt count':'sum', 'rcv stmt count':'sum',
            'rcv stmt (with select) count': 'sum', 'close stmt count': 'sum'}).sum(axis=1).mean(axis=0)
         nb_line_executed_in_goroutines = data_goroutines.iloc[:, -1:].sum(axis=0).sum(axis=0)
         total_nb_of_lines = data_lines.iloc[:, -1:].sum(axis=0).sum(axis=0)
         ratio_goroutines = (nb_line_executed_in_goroutines / total_nb_of_lines if total_nb_of_lines != 0 else 0)
         if nb_line_executed_in_goroutines != 0:
            proj_with_one_goroutine += 1
         if nb_channels != 0:
            proj_with_one_chan += 1
         is_using_channel = True if nb_channels > 0 else False
         is_using_goroutine = True if nb_line_executed_in_goroutines > 0 else False

         nb_channels_per_line = (nb_channels / total_nb_of_lines) if total_nb_of_lines > 0 else 0
         nb_channels_per_fct = (nb_channels / nb_functions)  if nb_functions > 0 else 0
         nb_channels_per_fct__line = ( nb_channels_per_fct / total_nb_of_lines) if total_nb_of_lines > 0 else 0
         nb_chan_operations_per_line = total_nb_of_operations / total_nb_of_lines  if total_nb_of_lines > 0 else 0
         coef =  total_nb_of_lines / avg_nb_of_line_go
         nb_channels_avg_nb_of_line = nb_channels * coef
         """
                  data = [{'filename': dirpath,
                           'Language': "go",
                           'nb_chan_per_proj': nb_channels,
                           'nb_chan_per_func_per_proj':(nb_channels / nb_functions), # divide number of channels by number of functions to get the average per function
                           'nb_chan_operations_per_proj': total_nb_of_operations,
                           'nb_chan_operations_per_func_per_proj': avg_nb_of_operations_per_func,
                           'percentage_executed_in_goroutines': (ratio_goroutines)*100,
                           'use_channel': is_using_channel,
                           'use_thread_or_goroutine': is_using_goroutine,
                           'nb_chan_per_line' : nb_channels_per_line}]"""

         data = [{'filename': dirpath,
                 'Language': "go",
                 'nb_chan_per_avg_nb_of_line': nb_channels_avg_nb_of_line,
                 'nb_chan_per_func_per_line': nb_channels_per_fct__line,
                 'nb_chan_operations_per_line': nb_chan_operations_per_line,
                 'nb_chan_operations_per_func_per_proj': avg_nb_of_operations_per_func,
                 'percentage_executed_in_goroutines': (ratio_goroutines) * 100,
                 'use_channel': is_using_channel,
                 'use_thread_or_goroutine': is_using_goroutine}]
         df = pd.concat([df, pd.DataFrame(data)], axis=0, join='outer', ignore_index=True, keys=None,
                        levels=None, names=None, verify_integrity=False, copy=True)
      else:
         print("error on : " + dirpath)

# Rust Data
total_nb_proj_rust = sum(1 for x in  os.walk('output-rust'))
total_nb_line_rust = 0
for dirpath, _, _ in os.walk('output-rust'):
   if dirpath != 'output-rust':
       data_lines = pd.read_csv(dirpath + "/fct2.csv")
       total_nb_line_rust += data_lines["function length (nb of lines)"].sum()
avg_nb_of_line_rust = total_nb_line_rust / total_nb_proj_rust
print("avg line per proj, go: ", avg_nb_of_line_rust)
print("total_nb lines rust", total_nb_line_rust)
for dirpath, _, _ in os.walk('output-rust'):
   if dirpath != 'output-rust':
      data_per_channel = pd.read_csv(dirpath + "/chan.csv")
      data_lines = pd.read_csv(dirpath + "/fct2.csv")
      functions_lenght = pd.read_csv(dirpath + "/fct1.csv")
      data_thread = pd.read_csv(dirpath + "/thread.csv")

      nb_functions = functions_lenght.shape[0]

      if nb_functions != 0:
         total_nb_proj += 1
         nb_channels = data_per_channel.shape[0] # get number of rows in file. Because 1 row = 1 channel
         total_nb_of_operations = data_per_channel["Send"].sum() + data_per_channel["Receive"].sum() + data_per_channel["Send in for loop"].sum() + data_per_channel["Receive in for loop"].sum()
         avg_nb_of_operations_per_func = data_per_channel.groupby(['channel Sender nam', 'channel Receiver nam']).agg({
            'Send':'sum', 'Send in for loop':'sum', 'Receive':'sum', 'Receive in for loop':'sum'}).sum(axis=1).mean(axis=0)
         total_nb_of_lines = data_lines["function length (nb of lines)"].sum()
         nb_line_executed_in_thread = data_thread["Thread_len"].sum()
         ratio_thread = (nb_line_executed_in_thread / total_nb_of_lines if total_nb_of_lines != 0 else 0)
         if nb_channels != 0:
            proj_with_one_chan += 1
         is_using_channelR = True if nb_channels > 0 else False
         is_using_thread = True if nb_line_executed_in_thread > 0 else False
         try:
             data_dependencies = pd.read_csv(dirpath + "/dependencies.csv")
             if data_dependencies["crossbeam-channel"].values[0] or data_dependencies["chan"].values[0] or \
                     data_dependencies["lasso-channel"].values[0] or data_dependencies["smartpool"].values[0]:
                is_using_channelR = True
             if data_dependencies["os-thread-local"].values[0]:
                is_using_thread = True
         except:
            pass
         nb_channels_per_line = (nb_channels / total_nb_of_lines) if total_nb_of_lines > 0 else 0
         nb_channels_per_fct = (nb_channels / nb_functions)  if nb_functions > 0 else 0
         nb_channels_per_fct__line = ( nb_channels_per_fct / total_nb_of_lines) if total_nb_of_lines > 0 else 0
         nb_chan_operations_per_line = total_nb_of_operations / total_nb_of_lines  if total_nb_of_lines > 0 else 0
         coef =  total_nb_of_lines / avg_nb_of_line_rust
         nb_channels_avg_nb_of_line = nb_channels * coef
         """         data = [{'filename': dirpath,
                           'Language': "rust",
                           'nb_chan_per_proj': nb_channels,
                           'nb_chan_per_func_per_proj':(nb_channels / nb_functions), # divide number of channels by number of functions to get the average per function
                           'nb_chan_operations_per_proj': total_nb_of_operations,
                           'nb_chan_operations_per_func_per_proj': avg_nb_of_operations_per_func,
                           'percentage_executed_in_goroutines': (ratio_thread) * 100,
                           'use_channel': is_using_channelR,
                           'use_thread_or_goroutine': is_using_thread,
                           'nb_chan_per_line' : nb_channels_per_line}]"""
         data = [{'filename': dirpath,
             'Language': "rust",
             'nb_chan_per_avg_nb_of_line': nb_channels_avg_nb_of_line,
             'nb_chan_per_func_per_line': nb_channels_per_fct__line,
             'nb_chan_operations_per_line': nb_chan_operations_per_line,
             'nb_chan_per_func_per_proj': avg_nb_of_operations_per_func,
             'percentage_executed_in_goroutines': (ratio_thread) * 100,
             'use_channel': is_using_channelR,
             'use_thread_or_goroutine': is_using_thread}]
         df = pd.concat([df, pd.DataFrame(data)], axis=0, join='outer', ignore_index=True, keys=None,
                        levels=None, names=None, verify_integrity=False, copy=True)
      else:
         print("error on : " + dirpath)

df['nb_chan_per_avg_nb_of_line'].describe()

boxplot = df.boxplot(by="Language", column='nb_chan_per_avg_nb_of_line')  #
boxplot2 = df.boxplot(by="Language", column='nb_chan_per_func_per_line') #
boxplot3 = df.boxplot(by="Language", column='nb_chan_operations_per_line') # per line
boxplot4 = df.boxplot(by="Language", column='nb_chan_per_func_per_proj') #
boxplot5 = df.boxplot(by="Language", column='percentage_executed_in_goroutines')
"""boxplot = df.boxplot(by="Language", column='nb_chan_per_proj')  #
boxplot2 = df.boxplot(by="Language", column='nb_chan_per_func_per_proj') #
boxplot3 = df.boxplot(by="Language", column='nb_chan_operations_per_line') # per line
boxplot4 = df.boxplot(by="Language", column='nb_chan_operations_per_func_per_proj') #
boxplot5 = df.boxplot(by="Language", column='percentage_executed_in_goroutines')
boxplot6 = df.boxplot(by="Language", column='nb_chan_per_line', ax=ax)
# boxplot6 = df[df['percentage_executed_in_goroutines'] > 0.0].boxplot(by="Language", column='percentage_executed_in_goroutines')
"""
plt.show()
df.to_csv(r'result.csv')


# RQ2 : Are message passing operations used more often in Go/Rust ?
# -> Is there more channels in go or in rust ?
# nombre de channels par projet ----> en go / en rust
# nombre de channels par fonction (en prenant en compte tous les projets) moyenne sur chaque projet  -----> en go / en rust
# -> Are message passing operations (send, receive, select, close) used more often in Go/Rust ?
# nombre moyen de (somme send, receive, select, etc) par channel, par projet
# nombre moyen de (somme send, receive, select, etc) par channel, par fonction


# RQ3 : Is Asynchronous programming used more often in Go/Rust ? Il faudra expliquer pourquoi cette question ? Quel est le rapport avec les message passing operations
# nombre de lignes executées dans des goroutines / nombre total de lignes du projet
# (nombre de fonctions executées dans des goroutines /

#RQ1 : script a faire
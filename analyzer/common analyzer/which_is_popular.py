from datetime import datetime

# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in

import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import seaborn as sns
import bq_helper

# ---------------Data on the nb of projects per Language-----------------
print("Script start time:", datetime.now().strftime("%H:%M:%S"))

query_nb_projects = """SELECT Rust_total_pj, Go_total_pj
FROM 
(SELECT count(*) FROM `bigquery-public-data.github_repos.languages`, UNNEST(language) as l where l.name LIKE "Rust" ) as Rust_total_pj,
(SELECT count(*) FROM `bigquery-public-data.github_repos.languages`, UNNEST(language) as l where l.name LIKE "Go" ) as Go_total_pj
 """
github_repos = bq_helper.BigQueryHelper(active_project="bigquery-public-data",
                                        dataset_name="github_repos")
Data_nb_projects = github_repos.query_to_pandas(query_nb_projects)

print(Data_nb_projects)
Data_nb_projects.to_csv(r'Data_nb_projects.csv', header=None, index=None, sep=' ', mode='a')

# ---------------Data on the nb of commit per Language-----------------
print("Data on the nb of commit per Language, start time:", datetime.now().strftime("%H:%M:%S"))

query_nb_commit_go = """SELECT count(com)  as nb_commit_Go 
FROM (SELECT * FROM `bigquery-public-data.github_repos.languages`, UNNEST(language) as l where l.name LIKE "Go") as lang
INNER JOIN `bigquery-public-data.github_repos.commits` AS com ON (lang.repo_name  IN UNNEST(com.repo_name))
 """
github_repos = bq_helper.BigQueryHelper(active_project="bigquery-public-data",
                                        dataset_name="github_repos")
Data_nb_commit_go = github_repos.query_to_pandas(query_nb_commit_go)  # Long execution tim due to the join

print(Data_nb_commit_go)
Data_nb_commit_go.to_csv(r'Data_nb_commit_go.csv', header=None, index=None, sep=' ', mode='a')

query_nb_commit_Rust = """SELECT count(com)  as nb_commit_Rust 
FROM (SELECT * FROM `bigquery-public-data.github_repos.languages`, UNNEST(language) as l where l.name LIKE "Rust") as lang
INNER JOIN `bigquery-public-data.github_repos.commits` AS com ON (lang.repo_name  IN UNNEST(com.repo_name))
 """
github_repos = bq_helper.BigQueryHelper(active_project="bigquery-public-data",
                                        dataset_name="github_repos")
Data_nb_commit_Rust = github_repos.query_to_pandas(query_nb_commit_Rust)  # Long execution tim due to the join

print(Data_nb_commit_Rust)
Data_nb_commit_Rust.to_csv(r'Data_nb_commit_Rust.csv', header=None, index=None, sep=' ', mode='a')

# ---------------Data on the nb of commit per year per language-----------------
print("Data on the nb of commit per year per language, start time:", datetime.now().strftime("%H:%M:%S"))

query_nb_commit_go_per_year = """SELECT COUNT(name) as nb_commit_go_per_year, Year
FROM (SELECT EXTRACT(YEAR FROM TIMESTAMP_SECONDS(com.committer.time_sec)) as Year, com.commit as name 
  FROM (SELECT * FROM `bigquery-public-data.github_repos.languages`, UNNEST(language) as l where l.name LIKE "Go") as lang
  INNER JOIN `bigquery-public-data.github_repos.commits` AS com ON (lang.repo_name  IN UNNEST(com.repo_name))
  ) 
GROUP By year
"""
github_repos = bq_helper.BigQueryHelper(active_project="bigquery-public-data",
                                        dataset_name="github_repos")
Data_nb_commit_go_per_year = github_repos.query_to_pandas(query_nb_commit_go_per_year)  # Long execution tim due to the join

print(Data_nb_commit_go_per_year)
Data_nb_commit_go_per_year.to_csv(r'Data_nb_commit_go_per_year.csv', header=None, index=None, sep=' ', mode='a')

query_nb_commit_Rust_per_year = """SELECT COUNT(name) as nb_commit_rust_per_year, Year
FROM (SELECT EXTRACT(YEAR FROM TIMESTAMP_SECONDS(com.committer.time_sec)) as Year, com.commit as name 
  FROM (SELECT * FROM `bigquery-public-data.github_repos.languages`, UNNEST(language) as l where l.name LIKE "Rust") as lang
  INNER JOIN `bigquery-public-data.github_repos.commits` AS com ON (lang.repo_name  IN UNNEST(com.repo_name))
  ) 
GROUP By year
"""
github_repos = bq_helper.BigQueryHelper(active_project="bigquery-public-data",
                                        dataset_name="github_repos")
Data_nb_commit_Rust_per_year = github_repos.query_to_pandas(query_nb_commit_Rust_per_year)  # Long execution tim due to the join

print(Data_nb_commit_Rust_per_year)
Data_nb_commit_Rust_per_year.to_csv(r'Data_nb_commit_Rust_per_year.csv', header=None, index=None, sep=' ', mode='a')

# ---------------Data on the nb of pj per year per language-----------------
print("Data on the nb of pj per year per language, start time:", datetime.now().strftime("%H:%M:%S"))

query_nb_pj_go_per_year = """SELECT COUNT(DISTINCT l_repo) as nb_pj_go_per_year, Year
FROM (SELECT EXTRACT(YEAR FROM TIMESTAMP_SECONDS(com.committer.time_sec)) as Year, com.repo_name  as com_rep_name 
  FROM (SELECT * FROM `bigquery-public-data.github_repos.languages`, UNNEST(language) as l where l.name LIKE "Go") as lang
  INNER JOIN `bigquery-public-data.github_repos.commits` AS com ON (lang.repo_name  IN UNNEST(com.repo_name))
  ), UNNEST(com_rep_name) as l_repo
GROUP By Year
"""
github_repos = bq_helper.BigQueryHelper(active_project="bigquery-public-data",
                                        dataset_name="github_repos")
Data_nb_pj_go_per_year = github_repos.query_to_pandas(query_nb_pj_go_per_year)  # Long execution tim due to the join

print(Data_nb_pj_go_per_year)
Data_nb_pj_go_per_year.to_csv(r'Data_nb_pj_go_per_year.csv', header=None, index=None, sep=' ', mode='a')


query_nb_pj_Rust_per_year = """SELECT COUNT(DISTINCT l_repo) as nb_pj_Rust_per_year, Year
FROM (SELECT EXTRACT(YEAR FROM TIMESTAMP_SECONDS(com.committer.time_sec)) as Year, com.repo_name  as com_rep_name 
  FROM (SELECT * FROM `bigquery-public-data.github_repos.languages`, UNNEST(language) as l where l.name LIKE "Rust") as lang
  INNER JOIN `bigquery-public-data.github_repos.commits` AS com ON (lang.repo_name  IN UNNEST(com.repo_name))
  ), UNNEST(com_rep_name) as l_repo
GROUP By Year"""
github_repos = bq_helper.BigQueryHelper(active_project="bigquery-public-data",
                                        dataset_name="github_repos")
Data_nb_pj_Rust_per_year = github_repos.query_to_pandas(query_nb_pj_Rust_per_year)  # Long execution tim due to the join

print(Data_nb_pj_Rust_per_year)
Data_nb_pj_Rust_per_year.to_csv(r'Data_nb_pj_Rust_per_year.csv', header=None, index=None, sep=' ', mode='a')


print("Script finish at:", datetime.now().strftime("%H:%M:%S"))
from Channel import *
method_list = []
method_call_list = []
thread_list = []


class Parameter:
    def __init__(self, name, type):
        self.name = name
        self.type = type
        self.called = 0
        self.calledInForLoop = 0

    def toString(self):
        return "\n\t\t{0} of type {1}, called: {2} ".format(self.name, self.type, self.called)

    def addCalled(self, nb):
        self.called += nb

    def addCalledInForLoop(self, nb):
        self.calledInForLoop += nb

    def toListFct3(self):
        l = [self.name, self.name, self.type, self.called, self.calledInForLoop]
        return l

class Function:
    def __init__(self, name, path):
        self.name = name
        self.parameters_chan = []
        self.call_list = []
        self.len = 0
        self.completeName = path

    def __str__(self):
        string = "Fn {0} called {1} times of len {2}.".format(self.completeName, len(self.call_list), self.len)
        for param in self.parameters_chan:
            string += "" + param.toString()
        string += "{"
        for calledby in self.call_list:
            string += calledby + ";"
        string += "}"

        return string


    def toListFct1(self):
        l = list()

        l.append(self.name)
        countSend = 0
        countSendForLoop = 0
        countReceive = 0
        countReceiveForLoop = 0
        for param in self.parameters_chan:
            if param.type == "Sender":
                countSend += param.called
                countSendForLoop +=param.calledInForLoop
            if param.type == "Receiver":
                countReceive += param.called
                countReceiveForLoop += param.calledInForLoop
        l.extend([countSend, countSendForLoop, countReceive, countReceiveForLoop])
        return l


    def toListFct2(self):
        l = list()

        l.append(self.name)
        l.append(self.len)
        return l

    def containsChannel(self):
        if len(self.parameters_chan) > 0:
            return True
        return False

    """Add the calling Function in the Function call list + add the number of call to the corresponding channel"""
    def CalledBy(self, method, fields):
        if fields is not None:
            for field in fields:
                try:
                    name = field[0]["kind"]["fields"][1]["segments"][0]["ident"]["name"]
                    if name != self.name:
                        method += "." + name
                except:
                    continue
        self.call_list.append(method)

    """Add the calling Function in the Function call list + add the number of call to the corresponding channel"""

    def addChannelParam(self, input):
        try:
            type = input["ty"]["kind"]["fields"][1]["segments"][1]["ident"]["name"]
            name = input["pat"]["kind"]["fields"][1]["name"]
            self.parameters_chan.append(Parameter(name, type))
        except:
            pass

    def getParams(self):
        return self.parameters_chan

    def getParam(self, paramName):
        for index, param in enumerate(self.getParams()):
            if param.name == paramName:
                return self.getParams()[index]
        return None


def addChannelsCounters(method, list_channels):
    param_list = method.getParams()
    if len(list_channels) != len(param_list):
        print("ERROR: parameters number (of type chan) and number of Functions param are diff")
        return
    for i, chan in enumerate(list_channels, start=0):
        if param_list[i].type == "Sender":
            chan.countSend += param_list[i].called
            chan.countSendForLoop += param_list[i].calledInForLoop
        if param_list[i].type == "Receiver":
            chan.countReceive += param_list[i].called
            chan.countReceiveForLoop += param_list[i].calledInForLoop


def getMethod(method_list, name):
    for method in method_list:
        if method.name == name:
            return method
    return None


def getParamMethod(method_list, method_name, param_type, param_name):
    method = getMethod(method_list, method_name)
    param_list = method.getParams()
    for param in param_list:
        if param.name == param_name and param.type == param_type:
            return param
    return None

def search_channel_params(fn):
    try:
        global method_list
        parameters = fn["kind"]["fields"][0]["decl"]["inputs"]
        for param in parameters:
            try:

                if isKindOfTypeChannel(param["ty"]["kind"]):
                    method = getMethod(method_list, fn["ident"]["name"])
                    if method is not None:
                        method.addChannelParam(param)
            except:
                pass
    except:
        pass


def calculate_channels():
    global method_list
    for fn in method_list:
        l = fn.call_list
        if len(l) > 0:
            print("fn {0} a appelé {1}".format(l, fn.name))
        for call in l:
            try:
                parsed = call.split('.')
                parsed_parameters = [x for x in parsed[1:]]
                targetFn = getMethod(method_list, parsed[0])
                for index, param in enumerate(parsed_parameters):
                    # TODO put in try catch for each param
                    l2 = targetFn.getParams()
                    target_param = targetFn.getParam(param)
                    if target_param is not None:
                        target_param.addCalled(fn.getParams()[index].called)
                        target_param.addCalledInForLoop(fn.getParams()[index].calledInForLoop)
                    else:
                        chan = getChan(param)
                        if chan is not None:
                            chan.addCounter(fn.getParams()[index].type, fn.getParams()[index].called, fn.getParams()[index].calledInForLoop)
                            #add value in the good category depending on type


            except:
                continue
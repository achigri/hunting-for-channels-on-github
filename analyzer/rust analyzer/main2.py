import csv

from github import Github, UnknownObjectException  # pip install PyGithub
import pathlib
import os
import pandas as pd
from os import listdir
from os.path import isfile, join, isdir
import sys
import toml
import glob

# Setup
if len(sys.argv) < 2:
    print("Usage: python3 main.py <path-to-go-root>")
    print("<path-to-go-root> : Path to the root directory of main.go file")
    exit(1)

g = Github("e8b58514616d35761b36c368a05b71b95ff4e04e")

df = pd.DataFrame()
BASEDIR = os.getcwd()
HOME = os.getenv("HOME")

pathlib.Path(HOME + "/hunting-rust/output/").mkdir(parents=True, exist_ok=True)

"""    try:
        f = open(sys.argv[2], "r")
        output = f.read()
        print("dependencies file: {0}".format(output))
    except:
        print("dependencies file cannot be read")"""
def analyse_project(folder, path):
    i = 0
    pathlib.Path(HOME + "/hunting-rust/output/" + folder).mkdir(parents=True, exist_ok=True)

    list_file = [f for f in listdir("{0}/{1}".format(path, folder)) if isfile(join("{0}/{1}".format(path, folder), f)) and ".json" in f]
    print("list files :", list_file)
    for file in list_file:
        try:
            print("python3 Main.py {2}/{3}/{0}".format(file, HOME, path, folder))
            os.system("python3 Main.py {2}/{3}/{0}".format(file, HOME, path, folder))
            os.system("mv fct1.csv {0}_fct1.csv".format(i))
            os.system("mv fct2.csv {0}_fct2.csv".format(i))
            os.system("mv fct3.csv {0}_fct3.csv".format(i))
            os.system("mv chan.csv {0}_chan.csv".format(i))
            os.system("mv thread.csv {0}_thread.csv".format(i))
        except:
            pass
        i += 1
    try:
        # Concat all csv
        list_fct1 = glob.glob('*_fct1.csv')
        combined_fct1 = pd.concat([pd.read_csv(f) for f in list_fct1])
        combined_fct1.to_csv(HOME + "/hunting-rust/output/" + folder + "/fct1.csv", index=False, encoding='utf-8-sig')

        list_fct2 = glob.glob('*_fct2.csv')
        combined_fct2 = pd.concat([pd.read_csv(f) for f in list_fct2])
        combined_fct2.to_csv(HOME + "/hunting-rust/output/" + folder + "/fct2.csv", index=False, encoding='utf-8-sig')

        list_fct3 = glob.glob('*_fct3.csv')
        combined_fct3 = pd.concat([pd.read_csv(f) for f in list_fct3])
        combined_fct3.to_csv(HOME + "/hunting-rust/output/" + folder + "/fct3.csv", index=False, encoding='utf-8-sig')

        list_chan = glob.glob('*_chan.csv')
        combined_chan = pd.concat([pd.read_csv(f) for f in list_chan])
        combined_chan.to_csv(HOME + "/hunting-rust/output/" + folder + "/chan.csv", index=False, encoding='utf-8-sig')

        list_thread = glob.glob('*_thread.csv')
        combined_thread = pd.concat([pd.read_csv(f) for f in list_thread])
        combined_thread.to_csv(HOME + "/hunting-rust/output/" + folder + "/thread.csv", index=False, encoding='utf-8-sig')
    except:
        print("Error with concatenation of csv files")
        pass

    # Create dependencies csv file
    #  Get channel library
    try:
        with open("{2}/{3}/dependencies.txt".format(file, HOME, path, folder)) as dependencies_f:
            dependencies_f_content = dependencies_f.read()
        dependency_crossbeam_chan = False
        dependency_chan = False
        dependency_ductile = False
        dependency_smartpool = False
        if "crossbeam-channel" in dependencies_f_content:
            dependency_crossbeam_chan = True
        if "chan" in dependencies_f_content:
            dependency_chan = True
        if "ductile" in dependencies_f_content:
            dependency_ductile = True
        if "smartpool" in dependencies_f_content:
            dependency_smartpool = True

        #  Get thread library
        dependency_lasso = False
        dependency_os_thread_local = False

        if "lasso-channel" in dependencies_f_content:
            dependency_lasso = True
        if "os-thread-local" in dependencies_f_content or "thread" in dependencies_f_content :
            dependency_os_thread_local = True
        dependencies_csv = [{'crossbeam-channel': dependency_crossbeam_chan,
                      'chan': dependency_chan,
                      'ductile': dependency_ductile,
                      'smartpool':dependency_smartpool,
                      'lasso-channel': dependency_lasso,
                      'os-thread-local': dependency_os_thread_local}]
        pd.concat([df, pd.DataFrame(dependencies_csv)], join='outer', ignore_index=True, keys=None,
                  levels=None, names=None, verify_integrity=False, copy=True).to_csv(r'' + HOME + '/hunting-rust/output/' + folder + '/dependencies.csv')
    except:
        print("Error with dependencies file")
        pass
    os.system("rm *.csv".format(i))

if len(sys.argv) == 4 and sys.argv[2] == "JSON_FOLDER":
    path = sys.argv[3]
    list_projects = [f for f in listdir(path) if isdir(join(path, f))]
    print("list_pj:", list_projects)
    for i, project in enumerate(list_projects):
        print("Analysing project {0}/{1}".format(i, len(list_projects)))
        pathlib.Path(HOME + "/hunting-rust/output/" + project).mkdir(parents=True, exist_ok=True)
        analyse_project(project, path)
        #os.system("python3 Main.py {2}/{0} JSON_FOLDER {2}/{0}".format(file, HOME, path))
        #os.system("mv *.csv {1}/hunting-rust/output/{2}".format(path, HOME, file.split(".")[0]))
        i += 1
    exit(0)


# Analysis in Rust

pathlib.Path("/tmp/hunting-rust/source").mkdir(parents=True, exist_ok=True)
pathlib.Path(HOME + "/hunting-rust/output").mkdir(parents=True, exist_ok=True)
res = g.search_repositories(query='language:Rust pushed:>2020-05-01 followers:>=500 stars:>=5000', sort="stars",
                            order="desc", )

os.chdir("/tmp/hunting-rust/source")
print("j'ai trouvé {0} repos correspondants aux critères.".format(res.totalCount))

selected_repo = []
i = 0
use_crossbeam = []


i = 0
for rep in res:
    if rep.name != "rust":
        try:
            rep.get_contents('Cargo.toml')
            selected_repo.append(rep)
            if i==9999:
                break
            i += 1
        except UnknownObjectException:
            print("repo: {0} ignored, cargo file not found".format(rep.name))


i = 0
print("Number of selected project (with cargo): {0}".format(len(selected_repo)))

def cargo_wrokspace(path_src, prev_path):
    try:
        print("Trying virtual workspace in: {0}".format(path_src))
        if path_src == prev_path:
            return
        try:
            print("try to open: {0}".format("/tmp/hunting-rust/source/" + path_src + "/Cargo.toml"))
            cargo = toml.load("/tmp/hunting-rust/source/" + path_src + "/Cargo.toml")
            if "dependencies" in cargo:
                cargo_dependencies = cargo["dependencies"]
                global dependencies
                dependencies.append(cargo_dependencies.keys())
            cargo_list = cargo["workspace"]["members"]
        except Exception as error:
            print("No workspace in this cargo file: ", error)  # No workspace, don't need to continue
            return
        for bin in cargo_list:
            if bin == ".":
                return
            try:
                print("cd /tmp/hunting-rust/source/" + path_src + "/" + bin + "&& cargo rustc -- -Z ast-json > outputScript.json".format(bin))

                result = os.system(
                    "cd /tmp/hunting-rust/source/" + path_src + "/" + bin + "&& cargo rustc -- -Z ast-json > outputScript.json".format(bin))
                if result != 0:  # if an error appear here It should be cargo workspace error
                    cargo_wrokspace(path_src + "/" + bin, path_src)
                else:  # If no error, we copy the result json
                    os.system(
                    "mv /tmp/hunting-rust/source/" + path_src + "/" + bin + "/outputScript.json" + " {1}/hunting-rust/output/{2}/{4}_{3}.json".format(
                        repo.name, HOME, name, bin.replace("/", "_"), path_src))
            except Exception as error:
                print("exception in compiling: {0}".format(error))
                continue
    except Exception as error:
        print("exception in compiling: ", error)
        print("Error: the project {0} cannot be compiled.".format(name))

for repo in selected_repo:
    if repo.name != "rust":
        global dependencies
        dependencies = []
        name = repo.full_name.replace("/", ".")
        pathlib.Path(HOME + "/hunting-rust/output/" + name).mkdir(parents=True, exist_ok=True)
        print("Cloning repository " + name)
        os.system("git clone " + repo.clone_url)
        print("Analysing repository " + name)
        print("cmd: " + "cd /tmp/hunting-rust/source/" + repo.name + " && cargo rustc -- -Z ast-json")

        result = os.system("cd /tmp/hunting-rust/source/" + repo.name + " && cargo rustc -- -Z ast-json > outputScript.json")

        print("result is: {0}".format(result))
        if result != 0: #if an error appear here It should be cargo workspace error
            a = repo.name
            cargo_wrokspace(a, "")
        try:
            print("Master Try to Open:" + "/tmp/hunting-rust/source/" + repo.name + "/Cargo.toml")
            cargo = toml.load("/tmp/hunting-rust/source/" + repo.name + "/Cargo.toml")
            cargo_dependencies = cargo["dependencies"]
            dependencies.append(cargo_dependencies.keys())
        except Exception as error:
            print("cannot get dependencies for {0}".format(repo.name))
            print(error)
            pass

        print(os.system("pwd"))
        print("-------------------------------------------------")
        for dep in dependencies:
            print(dep)
        print("-------------------------------------------------")
        with open("{1}/hunting-rust/output/{2}/dependencies.txt".format(repo.name, HOME, name), 'w', newline='') as file:
            for dep in dependencies:
                for line in dep:
                    file.write("{0}\n".format(line))
        #print("python analysis " + name)
        #os.system("python3 {1}/rust/Main.py /tmp/hunting-rust/source/{0}/outputScript.json".format(repo.name, sys.argv[1]))
        #os.system("mv /tmp/hunting-rust/source/*.csv {1}/hunting-rust/output/{0}".format(name, HOME, sys.argv[1]))
        #print("mv /tmp/hunting-rust/source/{0}/outputSrcipt.json {1}/hunting-rust/output/{0}.json".format(repo.name, HOME))
        os.system("mv /tmp/hunting-rust/source/{0}/outputScript.json {1}/hunting-rust/output/{2}/{0}.json".format(repo.name, HOME, name))
        os.system("rm -rf /tmp/hunting-rust/source/*")
        dependencies.clear()

os.system("rm -rf /tmp/hunting-rust")
print(
    str(res.totalCount) + " projects were analysed, you can find the results in " + HOME + "/hunting-rust/output folder")
os.chdir(BASEDIR)
chan_list = []

class Channel:
    def __init__(self, sender, receiver):
        self.sender = sender
        self.receiver = receiver
        self.countSend = 0
        self.countReceive = 0
        self.countReceiveForLoop = 0
        self.countSendForLoop = 0
        self.passedParam = []

    def addSend(self):
        self.countSend += 1

    def addSend(self, nb):
        self.countSend += nb

    def addReceive(self):
        self.countReceive += 1

    def addReceive(self, nb):
        self.countReceive += nb

    def addSendForLoop(self):
        self.countSendForLoop += 1

    def addSendForLoop(self, nb):
        self.countSendForLoop += nb

    def addReceiveForLoop(self):
        self.countReceiveForLoop += 1

    def addReceiveForLoop(self, nb):
        self.countReceiveForLoop += nb

    def __str__(self):
        return "channel ({0}, {1}), count {2} send and {3} receive \tincluding in for loop {4} send and {5} receive"\
            .format(self.sender, self.receiver, self.countSend, self.countReceive, self.countSendForLoop,
                    self.countReceiveForLoop)

    def addPassedParam(self, fnName):
        self.passedParam.append(fnName)

    def addCounter(self, type, nb, nbForloop):
        if type == "Sender":
            self.addSendForLoop(nbForloop)
            self.addSend(nb)
        else:
            self.addReceiveForLoop(nbForloop)
            self.addReceive(nb)

    def toListChan(self):
        return [self.sender, self.receiver, self.countSend, self.countSendForLoop, self.countReceive, self.countReceiveForLoop]

def getChan(name):
    global chan_list

    for chan in chan_list:
        if name == chan.sender or name == chan.receiver:
            return chan
    return None

def search_local_chan_create(line):
    try:
        fields = line["fields"][0]["init"]["kind"]["fields"]
        for field in fields:
            try:
                fields2 = field["kind"]["fields"]
                for field2 in fields2:
                    try:
                        if field2["segments"][0]["ident"]["name"] == "mpsc" and field2["segments"][1]["ident"]["name"] == "channel":
                            sender = line["fields"][0]["pat"]["kind"]["fields"][0][0]["kind"]["fields"][1]["name"]
                            receiver = line["fields"][0]["pat"]["kind"]["fields"][0][1]["kind"]["fields"][1]["name"]
                            global chan_list
                            chan_list.append(Channel(sender, receiver))
                    except:
                        pass
            except:
                pass
    except:
        pass


def isKindOfTypeChannel(kind):
    try:
        if (kind["fields"][1]["segments"][0]["ident"]["name"] == "mpsc" and kind["fields"][1]["segments"][1]["ident"]["name"] == "Sender") \
                or (kind["fields"][1]["segments"][0]["ident"]["name"] == "mpsc" and kind["fields"][1]["segments"][1]["ident"]["name"] == "Receiver"):
            return True

    except:
        pass
    return False


def getKindOfTypeChannel(kind):
    try:
        if (kind["fields"][1]["segments"][0]["ident"]["name"] == "mpsc") :
            return kind["fields"][1]["segments"][1]["ident"]["name"]

    except:
        pass
    return None
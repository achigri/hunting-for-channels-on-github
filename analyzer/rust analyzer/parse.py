from parseFunction import *

sends = 0
isSending = False
isReceiving = False
receives = 0
fncts_list = []
previous_name = ""
current_fn = "DEFAULT"
kind_list = []

def parcourt_items(items):
    global current_fn
    prev = None
    for item in items:
        if item is not None and isinstance(item, Iterable) and "kind" in item: # search kind
            try:
                if item["kind"]["variant"] == "Fn":
                    current_fn = item["ident"]["name"]
            except:
                continue

            if item is not None and isinstance(item, Iterable) and "kind" in item:  # search fields
                parcourt_kind(item["kind"])
        if item is not None and isinstance(item, Iterable) and "ident" in item: # search ident
            prev = parcourt_ident(item["ident"], prev)


def parcourt_kind(kind):
    global kind_list
    variant = False
    if kind is not None and "variant" in kind:  # search fields
        variant = True
        kind_list.append(kind["variant"])
        global previous_name
        global current_fn
        if kind["variant"] == "Call":
            try:
                method = None
                method_name = kind["fields"][0]["kind"]["fields"][1]["segments"][0]["ident"]["name"]
                if method_name != "$crate" and method_name != "mpsc":
                    method = getMethod(method_list, method_name)
                    if method is not None:
                        method.CalledBy(current_fn, kind["fields"])
                    if method_name == "thread":
                        print("j'ai un thread")
                        try:
                            thread_len = len(kind["fields"][1][0]["kind"]["fields"][4]["kind"]["fields"][0]["stmts"])
                            thread_list.append(thread_len)
                        except:
                            print("cannot count thread len")
            except:
                pass
        if kind["variant"] == "MethodCall":
            try:
                method_name = kind["fields"][0]["ident"]["name"]
                if method_name != "$crate" and method_name != "mpsc":
                    method = getMethod(method_list, method_name)
                    if method is not None:
                        method.CalledBy(current_fn, kind["fields"])
            except:
                pass

    if kind is not None and isinstance(kind, Iterable) and "fields" in kind:  # search fields
        parcourt_fields(kind["fields"])
    if variant:
        kind_list.pop()


def parcourt_smts(smts):
    for smt in smts:
        parcourt_kind(smt["kind"])


def parcourt_object(obj):
    for elem in obj:
        if elem is not None and isinstance(elem, Iterable) and "fields" in elem:  # search fields
            parcourt_fields(elem["fields"])
        elif elem is not None and isinstance(elem, Iterable) and "kind" in elem:  # search fields
            parcourt_kind(elem["kind"])
        elif elem is not None and isinstance(elem, Iterable) and "stmts" in elem:  # search fields
            parcourt_smts(elem["stmts"])
        elif elem is not None and isinstance(elem, dict):  # search init
            parcourt_object(elem)


def parcourt_array(array):
    for elem in array:
        if elem is not None and isinstance(elem, Iterable) and "fields" in elem:  # search fields
            parcourt_fields(elem["fields"])
        elif elem is not None and isinstance(elem, Iterable) and "kind" in elem:  # search fields
            parcourt_kind(elem["kind"])
        elif elem is not None and isinstance(elem, dict):  # search init
            parcourt_object(elem)


def parcourt_fields(fields):
    prev = None
    for field in fields:
        if field is not None and isinstance(field, str):
            continue
        if field is not None and isinstance(field, Iterable) and "init" in field:  # search init
            parcourt_init(field["init"])
        elif field is not None and isinstance(field, Iterable) and "kind" in field:  # search kind
            parcourt_kind(field["kind"])
        elif field is not None and isinstance(field, Iterable) and "segments" in field:  # search segments
            parcourt_segments(field["segments"])
        elif field is not None and isinstance(field, Iterable) and "stmts" in field:  # search segments
            parcourt_smts(field["stmts"])
        elif field is not None and isinstance(field, Iterable) and "ident" in field :  # search kind
            prev = parcourt_ident(field["ident"], prev)
        elif field is not None and isinstance(field, Iterable) and "items" in field:  # search items
            parcourt_items(field["items"])
        elif field is not None and isinstance(field, list):  # search init
            parcourt_array(field)


def parcourt_init(init):
    if init is not None and isinstance(init, Iterable) and "kind" in init:  # search kind
        parcourt_kind(init["kind"])

def parcourt_segments(segments):
    prev = None
    for seg in segments:
        if seg is not None and isinstance(seg, Iterable) and "ident" in seg:  # search kind
            prev = parcourt_ident(seg["ident"], prev)


def parcourt_ident(ident, previous):
    global isSending
    global isReceiving
    global kind_list

    if ident is not None and isinstance(ident, Iterable) and "name" in ident:
        global previous_name
        previous_name = ident["name"]
    if ident is not None and isinstance(ident, Iterable) and "name" in ident and isSending:
        isSending = False
        chan = getChan(ident["name"])
        if chan is not None:
            chan.addSend(1)
            if "ForLoop" in kind_list:
                chan.addSendForLoop(1)
        else:
            chan = getParamMethod(method_list, current_fn, "Sender", ident["name"])
            if chan is not None:
                chan.addCalled(1)
                if "ForLoop" in kind_list:
                    chan.addCalledInForLoop(1)

    if ident is not None and isinstance(ident, Iterable) and "name" in ident and isReceiving:
        isReceiving = False
        chan = getChan(ident["name"])
        if chan is not None:
            chan.addReceive(1)
            if "ForLoop" in kind_list:
                chan.addReceiveForLoop(1)
        else:
            chan = getParamMethod(method_list, current_fn, "Receiver", ident["name"])
            if chan is not None:
                chan.addCalled()
                if "ForLoop" in kind_list:
                    chan.addCalledInForLoop(1)


    if ident is not None and isinstance(ident, Iterable) and "name" in ident and ident["name"] == "send":
        global sends
        sends += 1
        isSending = True

    if ident is not None and isinstance(ident, Iterable) and "name" in ident and ident["name"] == "recv":
        global receives
        receives += 1
        isReceiving = True
    if "name" in ident:
        return ident["name"]
    else:
        return None



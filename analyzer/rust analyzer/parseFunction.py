from collections import Iterable

from Function import *


def parcourt_line(line):
    try:
        if line["kind"]["variant"] == "Local":
            search_local_chan_create(line["kind"])  # Search for a new channel created
    except:
        pass


def parsefunction(fn):
    try:
        for line in fn["kind"]["fields"][2]["stmts"]:
            parcourt_line(line)
    except:
        pass
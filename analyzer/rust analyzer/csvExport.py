import csv

def csvExportFct1(fct_list):
    with open('fct1.csv', 'w', newline='') as csvfile:
        file = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        file.writerow(["Function nam", "Send", "Send in for loop", "Receive", "Receive in for loop"])

        for fct in fct_list:
            file.writerow(fct.toListFct1())


def csvExportFct2(fct_list):
    with open('fct2.csv', 'w', newline='') as csvfile:
        file = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        file.writerow(["Function nam", "function length (nb of lines)"])

        for fct in fct_list:
            file.writerow(fct.toListFct2())

def csvExportFct3(fct_list):
    with open('fct3.csv', 'w', newline='') as csvfile:
        file = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        file.writerow(["Function nam", "function parameters"])

        for fct in fct_list:
            for param in fct.parameters_chan:
                file.writerow(param.toListFct3())

def csvExportChan(chan_list):
    with open('chan.csv', 'w', newline='') as csvfile:
        file = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        file.writerow(["channel Sender nam", "channel Receiver nam", "Send", "Send in for loop", "Receive", "Receive in for loop"])

        for chan in chan_list:
            file.writerow(chan.toListChan())


def csvExportThreads(thread_list):
    with open('thread.csv', 'w', newline='') as csvfile:
        file = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        file.writerow(["Thread_len"])

        for t in thread_list:
            file.writerow("{0}".format(t))
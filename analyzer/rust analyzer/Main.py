#!/usr/bin/python
import json
from parse import *
from searchFn import *
from csvExport import *
import sys
import os


def parcourt_items_main(items_list):
    for item in items_list:
        try:
            if item["kind"]["variant"] == "Fn":
                parsefunction(item)
                search_channel_params(item)
        except:
            pass


def main():
    f = open(sys.argv[1], "r")
    output = f.read()
    ast_tree = json.loads(output)
    items_list = ast_tree["module"]["items"]
    searchFn(ast_tree["module"], "")
    #parcourt_items_main(items_list)

    parcourt_items(items_list)
    global chan_list
    print("------------------------------------")
    calculate_channels()
    print("------------------------------------")

    """
    print("Functions/methods founds")
    for met in method_list:
        print(end="\t")
        print(met)"""

    print("for a total of :{0}".format(len(method_list)))

    if len(chan_list) > 0:
        print("channels founds")
        for chan in chan_list:
            print(end="\t")
            print(chan)
    else:
        print("no channel found")

    print("************************************")
    print("len thread founds:")
    for t in thread_list:
        print("\t{0}".format(t))
    print("************************************")

    print("csv folder is:" + os.getcwd())
    csvExportFct1(method_list)
    csvExportFct2(method_list)
    csvExportFct3(method_list)
    csvExportChan(chan_list)
    csvExportThreads(thread_list)

if __name__ == "__main__":
    main()
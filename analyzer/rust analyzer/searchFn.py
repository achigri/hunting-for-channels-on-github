from parseFunction import *


def searchFn(obj, path):
    try:
        if path != "":
            path += "." + obj["ident"]["name"]
        else:
            path += obj["ident"]["name"]
    except:
        pass

    try:
        if obj["kind"]["variant"] == "Fn":
            parsefunction(obj)
            try:
                method_list.append(Function(obj["ident"]["name"], path))
                m = getMethod(method_list, obj["ident"]["name"])
                for field in obj["kind"]["fields"]:
                    if "stmts" in field:
                        m.len = len(field["stmts"])
            except:
                pass
            search_channel_params(obj)
    except:
        pass

    try:
        if obj["kind"] != None:
            searchFn(obj["kind"], path)
    except:
        pass

    try:
        if obj["items"] != None:
            for item in obj["items"]:
                searchFn(item, path)
    except:
        pass

    try:
        if obj["fields"] != None:
            prev = None
            for field in obj["fields"]:
                try:
                    if "ident" in field[0]:
                        try:
                            name = prev["kind"]["fields"][1]["segments"][0]["ident"]["name"]

                            if path != "":
                                local_path = path + "." + name
                            else:
                                local_path = name
                        except:
                            pass
                        searchFn(field[0], local_path)
                except:
                    searchFn(field, path)
                prev = field
    except:
        pass

import re
import os

def count_lines(path):
   os.chdir(path)
   funcCounter = 0
   lineCounter = dict()

   for dirpath, dirs, files in os.walk(path):
      for filename in files:
         fname = os.path.join(dirpath, filename)
         try:
            size = os.stat(fname).st_size
         except:
            size = 1
         if fname.endswith('.go') and size < 1_000_000 and not fname.endswith('_test.go'):  # file size should be < 1MB
            with open(fname, 'r') as file:
               pack_name = None
               should_skip = False
               for line in file:
                  if re.search(r".*/\*.*$", line) and re.search(r".*\*/.*$", line):
                     if line.rfind("/*") > line.rfind("*/"):
                        should_skip = True
                     else:
                        should_skip = False
                  elif re.search(r".*/\*.*$", line):
                     should_skip = True
                  elif re.search(r".*\*/.*$", line):
                     should_skip = False
                  if not should_skip:
                     if re.search(r"\s*package\s*\w+.*$", line):
                        pack_name = re.search(r"\w+", line.replace("package", "")).group(0)
                     elif re.search(r"^\s*func\s*.*\w+\(.*\)\s*.*\s*{.*$", line) and pack_name is not None:
                        funcName = re.search(r"\w+\(", line).group(0).replace("(", "")
                        funcCounter += 1
                        counter = 0
                        ratio = 1
                        for line in file:
                           if re.search(r"^\s*//", line) or re.search(r"^\s*$", line):
                              pass
                           elif re.search("^.*}.*$",
                                          line) and ratio == 1:  # we expect to have ratio = 1 before closing because when the function will be close ratio - 1 will be equals to 0
                              counter += 1
                              lineCounter[pack_name + "." + funcName] = counter
                              break
                           elif re.search("^.*}.*{.*$", line):
                              counter += 1
                           elif re.search("^.*}.*$", line):
                              ratio -= 1
                              counter += 1
                           elif re.search("^.*{.*$", line):
                              ratio += 1
                              counter += 1
                           else:
                              counter += 1
                     elif re.search(r"^\s*func\s*.*\w+\(.*\)\s*.*\s*{\s*.*\s*}\s*$", line) and pack_name is not None:
                        funcName = re.search(r"\w+\(", line).group(0).replace("(", "")
                        funcCounter += 1
                        lineCounter[pack_name + "." + funcName] = 1
                     elif pack_name is None:
                        print("pack_name is None on file " + fname)

   result = calculate(lineCounter)
   try:
      with open("output/result-goroutines-lines.csv", 'w') as csvfile:
         csvfile.write('Function Name' + ", number of lines\n")
         for k, v in result.items():
            csvfile.write(k + ", " + str(v) + "\n")

      with open("output/number_of_functions.csv", 'w') as csvfile:
         csvfile.write('total number of functions' + ", " + str(funcCounter) + "\n")

      with open("output/result-lines.csv", 'w') as csvfile:
         csvfile.write('Function Name' + ", number of lines\n")
         for k, v in lineCounter.items():
            csvfile.write(k + ", " + str(v) + "\n")
   except IOError as e:
      print("I/O error")
      print(str(e))



def parse_funcCallsAndGoroutines():
   funcCalls = dict()
   goroutines = dict()
   with open("tmp/func_calls.csv", 'r') as file:
      for line in file:
         l = line.split(",")
         funcCalls[l[0]] = l[1:]

   with open("tmp/goroutines.csv", 'r') as file:
      for line in file:
         l = line.split(",")
         goroutines[l[0]] = l[1:]

   return funcCalls, goroutines


def calculate(lineCounter):
   funcCalls, goroutines = parse_funcCallsAndGoroutines()
   result = dict()
   for k, v in goroutines.items():
      try:
         result[k] = 0
         for func in v:
            func = func.replace("\n", "")
            if lineCounter[func]:
               result[k] += lineCounter[func]

            if funcCalls[func]:
               for called in funcCalls[func]:
                  called = called.replace("\n", "")
                  if lineCounter[called]:
                     result[k] += lineCounter[called]
      except Exception as e:
         print("Encountered exception " + str(e))

   return result
from github import Github # pip install PyGithub
import pathlib
import os
import sys
from .count_lines import count_lines

# Setup
if len(sys.argv) < 2:
   print("Usage: python3 main.py <path-to-go-root>")
   print("<path-to-go-root> : Path to the root directory of main.go file")
   exit(1)

g = Github("4972e91eaacb2c3c2e1e35e9be395debbe51fc83")

BASEDIR = os.getcwd()
HOME = os.getenv("HOME")

pathlib.Path("/tmp/hunting-go/source").mkdir(parents=True, exist_ok=True)
pathlib.Path(HOME + "/hunting-go/output").mkdir(parents=True, exist_ok=True)

#Build go exec
os.chdir(sys.argv[1])

os.putenv("GOPATH", sys.argv[1])

os.system("go build main.go")
os.system("mv main " + "/tmp/hunting-go/")

#Analysis in Go

res = g.search_repositories(query='language:Go pushed:>2020-05-01 followers:>=500 stars:>=5000', sort="stars", order="desc")

os.chdir("/tmp/hunting-go/source")
for repo in res:
   if repo.name != "go":
      name = repo.full_name.replace("/", ".")
      pathlib.Path(HOME + "/hunting-go/output/" + name).mkdir(parents=True, exist_ok=True)
      print("Cloning repository " + name)
      os.system("git clone " + repo.clone_url)
      print("Analysing repository " + name)
      os.system("cp /tmp/hunting-go/main /tmp/hunting-go/source/")
      os.system("./main " + "/tmp/hunting-go/source")
      print("python analysis " + name)
      count_lines("/tmp/hunting-go/source")
      os.system("mv ./output/* " + HOME + "/hunting-go/output/" + name)
      os.system("rm -rf /tmp/hunting-go/source/*")


os.system("rm -rf /tmp/hunting-go")
print(str(res.totalCount) + " projects were analysed, you can find the results in " + HOME + "/hunting-go/output folder")
os.chdir(BASEDIR)
package main

import (
	"encoding/csv"
	"fmt"
	"go/ast"
	"go/parser"
	"go/token"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
)

// fix lasPackageName & lastFuncName (associer ces variables à une goroutine)

var lastFuncName string
var lastPackageName string
var countByPackage = make(map[string]map[string]counter)
var funcParams = make(map[string][]*ast.Field)
var funcReturns = make(map[string]*ast.ReturnStmt)
var funcInformation = make(map[string] counter)
var totalMakeCount []string
var ignoredChannelCreation = 0

var goroutinesCall = make(map[string] []string)
var funcCalls = make(map[string] []string)

func main() {
	runtime.GOMAXPROCS(1)//runtime.NumCPU())

	if len(os.Args) < 2 {
		_, _ = fmt.Fprintf(os.Stderr, "usage:\n\t%s [files]\n", os.Args[0])
		os.Exit(1)
	}

	BASE_DIR, _ := os.Getwd()
	_ = os.Chdir(os.Args[1])

	fs := token.NewFileSet()
	// scan project a first time to get all channels declarations, etc
	runFiles(fs, false)
	// scan project a second time to collect metrics
	totalMakeCount = nil
	ignoredChannelCreation = 0
	funcInformation = make(map[string] counter)
	goroutinesCall = make(map[string] []string)
	funcCalls = make(map[string] []string)
	runFiles(fs, false)

	fmt.Println("--------------------------------------")
	channelsCount := 0
	for k, _ := range countByPackage {
		for range countByPackage[k] {
			channelsCount++
		}
	}
	fmt.Println("total channels numbers : " + strconv.Itoa(channelsCount))
	fmt.Println("total channels initialization numbers : " + strconv.Itoa(len(totalMakeCount)))
	fmt.Println("total ignored channels creations : " + strconv.Itoa(ignoredChannelCreation))
	fmt.Println(countByPackage)
	exportMap()
	_ = os.Chdir(BASE_DIR)
	fmt.Println("End")
}

func runFiles(fs *token.FileSet, print bool) {
	for _, arg := range os.Args[1:] {
		err := filepath.Walk(arg,
			func(path string, info os.FileInfo, err error) error {
				if err != nil {
					return err
				}
				if strings.HasSuffix(path, ".go") && !strings.HasSuffix(path, "_test.go") {
					if print {
						ParseFileByFilename(path)
					}
					log.Println("Started worker on filename = " + path)
					f, err := parser.ParseFile(fs, path, nil, parser.AllErrors)
					if err != nil {
						log.Printf("could not parse %s: %v", path, err)
					} else {
						v := newVisitor(f)
						ast.Walk(v, f)
					}
					log.Println("Finished worker on filename = " + path)
				} else {
					println("not analysing file " + path)
				}
				return nil
			})
		if err != nil {
			log.Println(err)
		}
	}

}

type visitor struct {
}

type counter struct {
	instantiated	 bool
	makeCount		 int
	sendStmtCount    int
	rcvStmtCount     int
	selectCount      int
	closeCount       int
	sendStmtCountFor int
	rcvStmtCountFor  int
	selectCountFor   int
	closeCountFor    int
}

func newVisitor(f *ast.File) visitor {
	decls := make(map[*ast.GenDecl]bool)
	for _, decl := range f.Decls {
		if v, ok := decl.(*ast.GenDecl); ok {
			decls[v] = true
		}
	}

	return visitor{}
}

func (v visitor) Visit(n ast.Node) ast.Visitor {
	if n == nil {
		return nil
	}
	switch d := n.(type) {
	case *ast.AssignStmt:

		for i, name := range d.Lhs {
			switch name.(type) {
			case *ast.Ident:
				funcName := ""
				switch d.Rhs[0].(type) {
				case *ast.CallExpr:
					switch d.Rhs[0].(*ast.CallExpr).Fun.(type) {
					case *ast.Ident:
						funcName = d.Rhs[0].(*ast.CallExpr).Fun.(*ast.Ident).Name
					case *ast.SelectorExpr:
						switch d.Rhs[0].(*ast.CallExpr).Fun.(*ast.SelectorExpr).X.(type) {
						case *ast.Ident:
							funcName = d.Rhs[0].(*ast.CallExpr).Fun.(*ast.SelectorExpr).X.(*ast.Ident).Name + "." + d.Rhs[0].(*ast.CallExpr).Fun.(*ast.SelectorExpr).Sel.Name
						case *ast.CallExpr:
							isCallExpr := true
							variable := d.Rhs[0].(*ast.CallExpr).Fun.(*ast.SelectorExpr)
							for isCallExpr {
								switch variable.X.(type) {
								case *ast.Ident:
									funcName = variable.X.(*ast.Ident).Name + "." + variable.Sel.Name
									isCallExpr = false
								case *ast.CallExpr:
									switch variable.X.(*ast.CallExpr).Fun.(type) {
									case *ast.SelectorExpr:
										variable = variable.X.(*ast.CallExpr).Fun.(*ast.SelectorExpr)
									case *ast.Ident:
										funcName = lastPackageName + "." + variable.X.(*ast.CallExpr).Fun.(*ast.Ident).Name
										isCallExpr = false
									default:
										println("error 206")
										isCallExpr = false
									}
								case *ast.SelectorExpr:
									isSelector := true
									var2 := variable.X.(*ast.SelectorExpr)
									for isSelector {
										switch var2.X.(type) {
										case *ast.Ident:
											funcName = var2.X.(*ast.Ident).Name + "." + var2.Sel.Name
											isCallExpr = false
											isSelector = false
										case *ast.SelectorExpr:
											var2 = var2.X.(*ast.SelectorExpr)
										default:
											println("err 223")
											isCallExpr = false
											isSelector = false
										}
									}

								default:
									println("error 227")
									isCallExpr = false
								}
							}
						}
					}
				}
				if funcName == "make" {
					channelName := name.(*ast.Ident).Name
					addChanToCounter(lastPackageName, lastFuncName, channelName)
					tmp := countByPackage[lastPackageName][lastFuncName+"."+channelName]
					tmp.makeCount++
					countByPackage[lastPackageName][lastFuncName+"."+channelName] = tmp
					tmp = funcInformation[lastPackageName+"."+lastFuncName]
					tmp.makeCount++
					funcInformation[lastPackageName+"."+lastFuncName] = tmp
					totalMakeCount = append(totalMakeCount, channelName)
				} else if funcReturns[funcName] != nil {
					if len(funcReturns[funcName].Results) == len(d.Lhs) {
						println("C'est OK")
						switch funcReturns[funcName].Results[i].(type) {
						case *ast.CallExpr:
							switch funcReturns[funcName].Results[i].(*ast.CallExpr).Fun.(type) {
							case *ast.Ident:
								if funcReturns[funcName].Results[i].(*ast.CallExpr).Fun.(*ast.Ident).Name == "make" {
									switch d.Lhs[i].(type) {
									case *ast.Ident:
										if d.Lhs[i].(*ast.Ident).Name != "_" {
											channelName := d.Lhs[i].(*ast.Ident).Name
											addChanToCounter(lastPackageName, lastFuncName, channelName)
											tmp := countByPackage[lastPackageName][lastFuncName+"."+channelName]
											tmp.makeCount++
											countByPackage[lastPackageName][lastFuncName+"."+channelName] = tmp
											tmp = funcInformation[lastPackageName+"."+lastFuncName]
											tmp.makeCount++
											funcInformation[lastPackageName+"."+lastFuncName] = tmp
											totalMakeCount = append(totalMakeCount, channelName)
										} else {
											ignoredChannelCreation++
										}
									}
								}

							}
						}
					} else {
						println("Non géré bis")
					}

				}
			}

		}
	case *ast.SendStmt:
		chanName := "error"
		switch d.Chan.(type) {
		case *ast.Ident:
			chanName = d.Chan.(*ast.Ident).Name
		case *ast.SelectorExpr:
			isSelector := true
			variable := d.Chan.(*ast.SelectorExpr)
			for isSelector {
				switch variable.X.(type) {
				case *ast.Ident:
					chanName = variable.X.(*ast.Ident).Name + "->" + variable.Sel.Name
					isSelector = false
				case *ast.SelectorExpr:
					variable = variable.X.(*ast.SelectorExpr)
				default:
					println("error 295")
					isSelector = false
				}
			}

		}
		tmp := countByPackage[lastPackageName][lastFuncName+"."+chanName]
		tmp.sendStmtCount++
		countByPackage[lastPackageName][lastFuncName+"."+chanName] = tmp
		tmp = funcInformation[lastPackageName+"."+lastFuncName]
		tmp.sendStmtCount++
		funcInformation[lastPackageName+"."+lastFuncName] = tmp
	case *ast.UnaryExpr:
		chanName := "error"
		switch d.X.(type) {
		case *ast.Ident:
			chanName = d.X.(*ast.Ident).Name
		case *ast.SelectorExpr:
			isSelectExpr := true
			variable := d.X.(*ast.SelectorExpr)
			for isSelectExpr {
				switch variable.X.(type) {
				case *ast.Ident:
					chanName = variable.X.(*ast.Ident).Name + "->" + variable.Sel.Name
					isSelectExpr = false
				case *ast.SelectorExpr:
					variable = variable.X.(*ast.SelectorExpr)
				case *ast.CallExpr:
					switch variable.X.(*ast.CallExpr).Fun.(type) {
					case *ast.SelectorExpr:
						variable = variable.X.(*ast.CallExpr).Fun.(*ast.SelectorExpr)
					default:
						println("error 287")
						isSelectExpr = false
					}

				case *ast.IndexExpr:
					switch variable.X.(*ast.IndexExpr).X.(type) {
					case *ast.SelectorExpr:
						variable = variable.X.(*ast.IndexExpr).X.(*ast.SelectorExpr)
					case *ast.Ident:
						chanName = variable.X.(*ast.IndexExpr).X.(*ast.Ident).Name
						isSelectExpr = false
					default:
						println("error 292")
						isSelectExpr = false
					}
				default:
					println("error 319")
					isSelectExpr = false
				}
			}
		}
		tmp := countByPackage[lastPackageName][lastFuncName+"."+chanName]
		tmp.rcvStmtCount++
		countByPackage[lastPackageName][lastFuncName+"."+chanName] = tmp
		tmp = funcInformation[lastPackageName+"."+lastFuncName]
		tmp.rcvStmtCount++
		funcInformation[lastPackageName+"."+lastFuncName] = tmp
	case *ast.SelectStmt:
		l := d.Body.List
		for i := 0; i < len(l); i++ {
			switch l[i].(type) {
			case *ast.CommClause:
				switch l[i].(*ast.CommClause).Comm.(type) {
				case *ast.AssignStmt:
					for j := 0; j < len(l[i].(*ast.CommClause).Comm.(*ast.AssignStmt).Rhs); j++ {
						switch l[i].(*ast.CommClause).Comm.(*ast.AssignStmt).Rhs[j].(type) {
						case *ast.UnaryExpr:
							switch l[i].(*ast.CommClause).Comm.(*ast.AssignStmt).Rhs[j].(*ast.UnaryExpr).X.(type) {
							case *ast.Ident:
								tmp := countByPackage[lastPackageName][lastFuncName+"."+l[i].(*ast.CommClause).Comm.(*ast.AssignStmt).Rhs[j].(*ast.UnaryExpr).X.(*ast.Ident).Name]
								tmp.selectCount++
								countByPackage[lastPackageName][lastFuncName+"."+l[i].(*ast.CommClause).Comm.(*ast.AssignStmt).Rhs[j].(*ast.UnaryExpr).X.(*ast.Ident).Name] = tmp
								tmp = funcInformation[lastPackageName+"."+lastFuncName]
								tmp.selectCount++
								funcInformation[lastPackageName+"."+lastFuncName] = tmp
							}
						}
					}
				case *ast.ExprStmt:
					switch l[i].(*ast.CommClause).Comm.(*ast.ExprStmt).X.(type) {
					case *ast.UnaryExpr:
						switch l[i].(*ast.CommClause).Comm.(*ast.ExprStmt).X.(*ast.UnaryExpr).X.(type) {
						case *ast.Ident:
							tmp := countByPackage[lastPackageName][lastFuncName+"."+l[i].(*ast.CommClause).Comm.(*ast.ExprStmt).X.(*ast.UnaryExpr).X.(*ast.Ident).Name]
							tmp.selectCount++
							countByPackage[lastPackageName][lastFuncName+"."+l[i].(*ast.CommClause).Comm.(*ast.ExprStmt).X.(*ast.UnaryExpr).X.(*ast.Ident).Name] = tmp
							tmp = funcInformation[lastPackageName+"."+lastFuncName]
							tmp.selectCount++
							funcInformation[lastPackageName+"."+lastFuncName] = tmp
						}
					}
				}
			}
		}
	case *ast.FuncDecl:
		lastFuncName = d.Name.Name
		funcParams[lastPackageName+"."+d.Name.Name] = d.Type.Params.List
		funcInformation[lastPackageName+"."+d.Name.Name] = counter{
			instantiated:     true,
			makeCount: 		  0,
			sendStmtCount:    0,
			sendStmtCountFor: 0,
			rcvStmtCount:     0,
			rcvStmtCountFor:  0,
			selectCount:      0,
			selectCountFor:   0,
			closeCount:       0,
			closeCountFor:    0,
		}
		
		for i := 0; i < len(d.Type.Params.List); i++ {
			switch d.Type.Params.List[i].Type.(type) {
			case *ast.ChanType:
				if len(d.Type.Params.List[i].Names) > 0 {
					
					addChanToCounter(lastPackageName, lastFuncName, d.Type.Params.List[i].Names[0].Name)
					
				}
			}
		}
		break
	case *ast.File:
		
		lastPackageName = d.Name.Name
		countByPackage[lastPackageName] = make(map[string]counter)
		
	case *ast.GoStmt:
		switch d.Call.Fun.(type) {
		case *ast.Ident:
			
			goroutinesCall[lastPackageName+"."+lastFuncName] = append(goroutinesCall[lastPackageName+"."+lastFuncName], d.Call.Fun.(*ast.Ident).Name)
			
		case *ast.SelectorExpr:
			funcName := ""
			switch d.Call.Fun.(*ast.SelectorExpr).X.(type) {
			case *ast.Ident:
				funcName = d.Call.Fun.(*ast.SelectorExpr).X.(*ast.Ident).Name + "." + d.Call.Fun.(*ast.SelectorExpr).Sel.Name
				
				goroutinesCall[lastPackageName+"."+lastFuncName] = append(goroutinesCall[lastPackageName+"."+lastFuncName], funcName)
				
			}
		}
	case *ast.CallExpr:
		switch d.Fun.(type) {
		case *ast.Ident:
			
			funcCalls[lastPackageName + "." + lastFuncName] = append(funcCalls[lastPackageName + "." + lastFuncName], lastPackageName + "." + d.Fun.(*ast.Ident).Name)
			
			if d.Fun.(*ast.Ident).Name == "close" && len(d.Args) > 0{
				switch d.Args[0].(type) {
				case *ast.Ident:
					
					tmp := countByPackage[lastPackageName][lastFuncName+"."+d.Args[0].(*ast.Ident).Name]
					tmp.closeCount++
					countByPackage[lastPackageName][lastFuncName+"."+d.Args[0].(*ast.Ident).Name] = tmp
					tmp = funcInformation[lastPackageName+"."+lastFuncName]
					tmp.closeCount++
					funcInformation[lastPackageName+"."+lastFuncName] = tmp
					
				default:
					println("missed one 407")
				}
			}

			if d.Fun.(*ast.Ident).Obj != nil {
				switch d.Fun.(*ast.Ident).Obj.Decl.(type) {
				case *ast.FuncDecl:
					list := d.Fun.(*ast.Ident).Obj.Decl.(*ast.FuncDecl).Type.Params.List
					if _, ok := funcParams[lastPackageName +"."+ d.Fun.(*ast.Ident).Name]; ok {
						if len(funcParams[lastPackageName +"."+ d.Fun.(*ast.Ident).Name]) == len(list) {
							for i := 0; i < len(list); i++ {
								switch list[i].Type.(type) {
								case *ast.ChanType:
									switch d.Args[i].(type) {
									case *ast.Ident:
										
										addChanCountToOther(lastPackageName, d.Fun.(*ast.Ident).Name, d.Args[i].(*ast.Ident).Name, i)
										
									case *ast.SelectorExpr:
										switch d.Args[i].(*ast.SelectorExpr).X.(type) {
										case *ast.Ident:
											addChanCountToOther(lastPackageName, d.Fun.(*ast.Ident).Name, d.Args[i].(*ast.SelectorExpr).X.(*ast.Ident).Name + "->" + d.Args[i].(*ast.SelectorExpr).Sel.Name, i)
										default:
											println("error 440")
										}

									default:
										println("missed one 424")
									}
								default:
									println("error 441")
								}
							}
						} else {
							println("AYAYAYAYAYAYAYA")
						}
					}


				}
			}
		case *ast.SelectorExpr:
			packName := "error"
			funcName := "error"
			switch d.Fun.(*ast.SelectorExpr).X.(type) {
			case *ast.Ident:
				packName = d.Fun.(*ast.SelectorExpr).X.(*ast.Ident).Name
				funcName = d.Fun.(*ast.SelectorExpr).Sel.Name
			case *ast.CallExpr:
				switch d.Fun.(*ast.SelectorExpr).X.(*ast.CallExpr).Fun.(type) {
				case *ast.SelectorExpr:
					isCallExpr := true
					variable := d.Fun.(*ast.SelectorExpr).X.(*ast.CallExpr).Fun.(*ast.SelectorExpr)
					for isCallExpr {
						switch variable.X.(type) {
						case *ast.Ident:
							packName = variable.X.(*ast.Ident).Name
							funcName = variable.Sel.Name
							isCallExpr = false
						case *ast.CallExpr:
							switch variable.X.(*ast.CallExpr).Fun.(type) {
							case *ast.SelectorExpr:
								variable = variable.X.(*ast.CallExpr).Fun.(*ast.SelectorExpr)
							case *ast.Ident:
								packName = lastPackageName
								funcName = variable.X.(*ast.CallExpr).Fun.(*ast.Ident).Name
								isCallExpr = false
							default:
								print("error 459 removed")
								isCallExpr = false
							}
						case *ast.SelectorExpr:
							isSelectExpr := true
							var2 := variable.X.(*ast.SelectorExpr)
							for isSelectExpr {
								switch var2.X.(type) {
								case *ast.SelectorExpr:
									var2 = var2.X.(*ast.SelectorExpr)
								case *ast.Ident:
									packName = var2.X.(*ast.Ident).Name
									funcName = var2.Sel.Name
									isSelectExpr = false
									isCallExpr = false
								default:
									println("error")
									isSelectExpr = false
									isCallExpr = false
								}
							}
						case *ast.CompositeLit:
							isCallExpr = false
						default:
							print("error")
							isCallExpr = false
						}
					}
				case *ast.Ident:
					packName = lastPackageName
					funcName = d.Fun.(*ast.SelectorExpr).X.(*ast.CallExpr).Fun.(*ast.Ident).Name
				default:
					println("error 498")
				}
			default:
				println("error 499")
			}
			
			funcCalls[lastPackageName + "." + lastFuncName] = append(funcCalls[lastPackageName + "." + lastFuncName], packName + "." + funcName)
			if val, ok := funcParams[packName+"."+funcName]; ok {
				if len(val) == len(d.Args) {
					for count := 0; count < len(val); count++ {
						switch val[count].Type.(type) {
						case *ast.ChanType:
							switch d.Args[count].(type) {
							case *ast.Ident:
								addChanCountToOther(packName, funcName, d.Args[count].(*ast.Ident).Name, count)
							case *ast.SelectorExpr:
								isSelector := true
								variable := d.Args[count].(*ast.SelectorExpr)
								for isSelector {
									switch variable.X.(type) {
									case *ast.Ident:
										addChanCountToOther(packName, funcName, variable.X.(*ast.Ident).Name + "->" + variable.Sel.Name, count)
										isSelector = false
									case *ast.SelectorExpr:
										variable = variable.X.(*ast.SelectorExpr)
									default:
										println("error 554")
										isSelector = false
									}
								}

							default:
								println("missed one 539")
							}
						}
					}
				} else {
					println("Non géré")
				}
			}

			
		}
	case *ast.ReturnStmt:
		for i := range d.Results {
			switch d.Results[i].(type) {
			case *ast.CallExpr:
				switch d.Results[i].(*ast.CallExpr).Fun.(type) {
				case *ast.Ident:
					if d.Results[i].(*ast.CallExpr).Fun.(*ast.Ident).Name == "make" {
						// add function to funcReturns only if it return a new channel
						
						funcReturns[lastPackageName+"."+lastFuncName] = d
						
						break
					}
				}
			}
		}
	case *ast.ForStmt:
		for i := 0; i < len(d.Body.List); i++ {
			switch d.Body.List[i].(type) {
			case *ast.ExprStmt:
				switch d.Body.List[i].(*ast.ExprStmt).X.(type) {
				case *ast.UnaryExpr:
					switch d.Body.List[i].(*ast.ExprStmt).X.(*ast.UnaryExpr).X.(type) {
					case *ast.Ident:
						chanName := d.Body.List[i].(*ast.ExprStmt).X.(*ast.UnaryExpr).X.(*ast.Ident).Name
						
						tmp := countByPackage[lastPackageName][lastFuncName+"."+chanName]
						tmp.rcvStmtCountFor++
						countByPackage[lastPackageName][lastFuncName+"."+chanName] = tmp
						tmp = funcInformation[lastPackageName+"."+lastFuncName]
						tmp.rcvStmtCountFor++
						funcInformation[lastPackageName+"."+lastFuncName] = tmp
						
					}
				case *ast.CallExpr:
					switch d.Body.List[i].(*ast.ExprStmt).X.(*ast.CallExpr).Fun.(type) {
					case *ast.Ident:
						if d.Body.List[i].(*ast.ExprStmt).X.(*ast.CallExpr).Fun.(*ast.Ident).Name == "close" {
							switch d.Body.List[i].(*ast.ExprStmt).X.(*ast.CallExpr).Args[0].(type) {
							case *ast.Ident:
								
								tmp := countByPackage[lastPackageName][lastFuncName+"."+d.Body.List[i].(*ast.ExprStmt).X.(*ast.CallExpr).Args[0].(*ast.Ident).Name]
								tmp.closeCountFor++
								countByPackage[lastPackageName][lastFuncName+"."+d.Body.List[i].(*ast.ExprStmt).X.(*ast.CallExpr).Args[0].(*ast.Ident).Name] = tmp
								tmp = funcInformation[lastPackageName+"."+lastFuncName]
								tmp.closeCountFor++
								funcInformation[lastPackageName+"."+lastFuncName] = tmp
								
							case *ast.SelectorExpr:
								switch d.Body.List[i].(*ast.ExprStmt).X.(*ast.CallExpr).Args[0].(*ast.SelectorExpr).X.(type) {
								case *ast.Ident:
									tmp := countByPackage[lastPackageName][lastFuncName+"."+d.Body.List[i].(*ast.ExprStmt).X.(*ast.CallExpr).Args[0].(*ast.SelectorExpr).X.(*ast.Ident).Name + "->" + d.Body.List[i].(*ast.ExprStmt).X.(*ast.CallExpr).Args[0].(*ast.SelectorExpr).Sel.Name]
									tmp.closeCountFor++
									countByPackage[lastPackageName][lastFuncName+"."+d.Body.List[i].(*ast.ExprStmt).X.(*ast.CallExpr).Args[0].(*ast.SelectorExpr).X.(*ast.Ident).Name + "->" + d.Body.List[i].(*ast.ExprStmt).X.(*ast.CallExpr).Args[0].(*ast.SelectorExpr).Sel.Name] = tmp
									tmp = funcInformation[lastPackageName+"."+lastFuncName]
									tmp.closeCountFor++
									funcInformation[lastPackageName+"."+lastFuncName] = tmp
								default:
									println("err 618")
								}
							}
						}
					}
				}
			case *ast.SendStmt:
				switch d.Body.List[i].(*ast.SendStmt).Chan.(type) {
				case *ast.Ident:
					chanName := d.Body.List[i].(*ast.SendStmt).Chan.(*ast.Ident).Name
					
					tmp := countByPackage[lastPackageName][lastFuncName+"."+chanName]
					tmp.sendStmtCountFor++
					countByPackage[lastPackageName][lastFuncName+"."+chanName] = tmp
					tmp = funcInformation[lastPackageName+"."+lastFuncName]
					tmp.sendStmtCountFor++
					funcInformation[lastPackageName+"."+lastFuncName] = tmp
					
				}
			case *ast.SelectStmt:
				l := d.Body.List[i].(*ast.SelectStmt).Body.List
				for i := 0; i < len(l); i++ {
					switch l[i].(type) {
					case *ast.CommClause:
						switch l[i].(*ast.CommClause).Comm.(type) {
						case *ast.AssignStmt:
							for j := 0; j < len(l[i].(*ast.CommClause).Comm.(*ast.AssignStmt).Rhs); j++ {
								switch l[i].(*ast.CommClause).Comm.(*ast.AssignStmt).Rhs[j].(type) {
								case *ast.UnaryExpr:
									switch l[i].(*ast.CommClause).Comm.(*ast.AssignStmt).Rhs[j].(*ast.UnaryExpr).X.(type) {
									case *ast.Ident:
										
										tmp := countByPackage[lastPackageName][lastFuncName+"."+l[i].(*ast.CommClause).Comm.(*ast.AssignStmt).Rhs[j].(*ast.UnaryExpr).X.(*ast.Ident).Name]
										tmp.selectCountFor++
										countByPackage[lastPackageName][lastFuncName+"."+l[i].(*ast.CommClause).Comm.(*ast.AssignStmt).Rhs[j].(*ast.UnaryExpr).X.(*ast.Ident).Name] = tmp
										tmp = funcInformation[lastPackageName+"."+lastFuncName]
										tmp.selectCountFor++
										funcInformation[lastPackageName+"."+lastFuncName] = tmp
										
									}
								}
							}
						case *ast.ExprStmt:
							switch l[i].(*ast.CommClause).Comm.(*ast.ExprStmt).X.(type) {
							case *ast.UnaryExpr:
								switch l[i].(*ast.CommClause).Comm.(*ast.ExprStmt).X.(*ast.UnaryExpr).X.(type) {
								case *ast.Ident:
									
									tmp := countByPackage[lastPackageName][lastFuncName+"."+l[i].(*ast.CommClause).Comm.(*ast.ExprStmt).X.(*ast.UnaryExpr).X.(*ast.Ident).Name]
									tmp.selectCountFor++
									countByPackage[lastPackageName][lastFuncName+"."+l[i].(*ast.CommClause).Comm.(*ast.ExprStmt).X.(*ast.UnaryExpr).X.(*ast.Ident).Name] = tmp
									tmp = funcInformation[lastPackageName+"."+lastFuncName]
									tmp.selectCountFor++
									funcInformation[lastPackageName+"."+lastFuncName] = tmp
									
								}
							}
						}
					}
				}
			}
		}
	}
	return v
}

func addChanCountToOther(packName string, funcName string, sentArg string, argNb int) {
	if val, ok := funcParams[packName+"."+funcName]; ok {
		toAdd := countByPackage[packName][funcName+"."+val[argNb].Names[0].Name]

		tmp := countByPackage[lastPackageName][lastFuncName+"."+sentArg]
		tmp.makeCount += toAdd.makeCount
		tmp.sendStmtCount += toAdd.sendStmtCount
		tmp.sendStmtCountFor += toAdd.sendStmtCountFor
		tmp.rcvStmtCount += toAdd.rcvStmtCount
		tmp.rcvStmtCountFor += toAdd.rcvStmtCountFor
		tmp.selectCount += toAdd.selectCount
		tmp.selectCountFor += toAdd.selectCountFor
		tmp.closeCount += toAdd.closeCount
		tmp.closeCountFor += toAdd.closeCountFor
		countByPackage[lastPackageName][lastFuncName+"."+sentArg] = tmp
		println("Added " + packName + "." + funcName + "." + val[argNb].Names[0].Name + " count to variable " + lastPackageName + "." + lastFuncName + "." + sentArg)
	}
}

func addChanToCounter(packageName string, funcName string, chanName string) {
	if !countByPackage[packageName][funcName+"."+chanName].instantiated {
		countByPackage[packageName][funcName+"."+chanName] = counter{
			instantiated: true,
			makeCount: 		  0,
			sendStmtCount:    0,
			sendStmtCountFor: 0,
			rcvStmtCount:     0,
			rcvStmtCountFor:  0,
			selectCount:      0,
			selectCountFor:   0,
			closeCount:       0,
			closeCountFor:    0,
		}
	}
}

func ParseFileByFilename(filename string) {
	fs := token.NewFileSet()
	var v visitor
	f, err := parser.ParseFile(fs, filename, nil, parser.AllErrors)
	if err != nil {
		log.Fatal(err)
	}
	ast.Walk(v, f)
}

func exportMap() {
	_ = os.Mkdir("output", 0700)
	_ = os.Mkdir("tmp", 0700)
	data := [][]string{}
	data = append(data, []string{"package name", "function name", "variable name", "make count", "send stmt count", "rcv stmt count", "rcv stmt (with select) count", "close stmt count", "send in for loop", "rcv in for loop", "rcv stmt (with select) count in for loops", "close stmt count in for loops"})
	for package_name, info := range countByPackage {
		for variable_name, counter := range info {
			data = append(data, []string{package_name, strings.Split(variable_name, ".")[0], strings.Split(variable_name, ".")[1], strconv.Itoa(counter.makeCount), strconv.Itoa(counter.sendStmtCount), strconv.Itoa(counter.rcvStmtCount), strconv.Itoa(counter.selectCount), strconv.Itoa(counter.closeCount),
				strconv.Itoa(counter.sendStmtCountFor), strconv.Itoa(counter.rcvStmtCountFor), strconv.Itoa(counter.selectCountFor), strconv.Itoa(counter.closeCountFor)})
		}
	}
	if err := csvExport(data, "output/result-channels.csv"); err != nil {
		log.Fatal(err)
	}
	data = [][]string{}
	data = append(data, []string{"package name", "function name", "make count", "send stmt count", "rcv stmt count", "rcv stmt (with select) count", "close stmt count", "send in for loop", "rcv in for loop", "rcv stmt (with select) count in for loops", "close stmt count in for loops"})
	for function_name, counter := range funcInformation {
		data = append(data, []string{strings.Split(function_name, ".")[0], strings.Split(function_name, ".")[1], strconv.Itoa(counter.makeCount), strconv.Itoa(counter.sendStmtCount), strconv.Itoa(counter.rcvStmtCount), strconv.Itoa(counter.selectCount), strconv.Itoa(counter.closeCount),
			strconv.Itoa(counter.sendStmtCountFor), strconv.Itoa(counter.rcvStmtCountFor), strconv.Itoa(counter.selectCountFor), strconv.Itoa(counter.closeCountFor)})
	}
	if err := csvExport(data, "output/result-functions.csv"); err != nil {
		log.Fatal(err)
	}

	// goroutines
	data = [][]string{}
	for baseFunction, goroutines := range goroutinesCall {
		arr := []string{baseFunction}
		for _, funcName := range goroutines {
			arr = append(arr, funcName)
		}
		data = append(data, arr)
	}
	if err := csvExport(data, "tmp/goroutines.csv"); err != nil {
		log.Fatal(err)
	}
	data = [][]string{}
	for baseFunction, goroutines := range funcCalls {
		arr := []string{baseFunction}
		for _, funcName := range goroutines {
			arr = append(arr, funcName)
		}
		data = append(data, arr)
	}
	if err := csvExport(data, "tmp/func_calls.csv"); err != nil {
		log.Fatal(err)
	}
}

func csvExport(data [][]string, filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	for _, value := range data {
		if err := writer.Write(value); err != nil {
			return err // let's return errors if necessary, rather than having a one-size-fits-all error handler
		}
	}
	return nil
}

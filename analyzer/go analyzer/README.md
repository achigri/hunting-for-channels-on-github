# **Hunting for Channels on GitHub : Go project analysis**

## **How to launch the analysis ?**

### To run in the terminal, enter the command :
- `cd /go_code`
- `./analyze.sh `

### To run in background, enter the following commands:
- `cd /go_code`
- `screen`
- `./analyze.sh`\
Then press the shortcut `Ctrl-a + Ctrl-d` to detach your terminal from the screen session and thus, have the analysis executed in background.\
To re-attach the terminal to the screen session, run the command `screen -r`.


## Author
* **Aziz Chigri** - *Go projects Analysis* - *Visualisation script* - [Linkedin](https://www.linkedin.com/in/aziz-chigri/)
